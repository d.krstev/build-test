package com.propellor.update;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.api.services.sheets.v4.model.BatchUpdateValuesRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateValuesResponse;
import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.ImpersonatedCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;

import java.io.BufferedReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.ArrayList;

public class SheetsReader {
    private static final String APPLICATION_NAME = "Google Sheets API";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    private static final String USER_AGENT = "Mozilla/5.0";
    
    private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.SPREADSHEETS);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    public static void main(String... args) throws IOException, GeneralSecurityException {
        System.out.println("Running updater...");
        
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        
        // arguments passed from .gitlab-ci.yml
        final String spreadsheetId = args[0];
        final String range = args[1];
        final String version = args[2];
        final String project = args[3];
        final String versionNumber = args[4];
        final String jsonCreds = args[5];

        GoogleCredential googleCredential = GoogleCredential.fromStream(new ByteArrayInputStream(jsonCreds.getBytes(StandardCharsets.UTF_8)))
            .createScoped(Collections.singleton(SheetsScopes.SPREADSHEETS));
        
        Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, googleCredential)
                .setApplicationName(APPLICATION_NAME)
                .build();
        ValueRange response = service.spreadsheets().values()
                .get(spreadsheetId, range)
                .execute();
        List<List<Object>> values = response.getValues();
        List<List<Object>> newValues = new ArrayList();
        
        if (values == null || values.isEmpty()) {
            System.out.println("No data found.");
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String date = simpleDateFormat.format(new Date());

            for (List column : values) {
                try {
                    if (column.size() == 0)
                        break;

                    final String updateChunk = "public/deploy.cfm?source=git-ci&build=" + version + "&project=" + project;
                    
                    final String checkChunk = "site-check/status.cfm";
                    final String clientUrl = column.get(2).toString();
                    final String currentVersion = column.get(6).toString();
                    final boolean doUpdate = column.get(4).toString().equals("yes");

                    System.out.printf(ANSI_YELLOW + "%s - Perform update: " + doUpdate + ANSI_RESET + "\n", clientUrl);

                    if (doUpdate) {
                        // String deployClientUrl = clientUrl.replace("propellor/", checkChunk);
                        String deployClientUrl = clientUrl + updateChunk;
                        String resp = sendRequest(deployClientUrl);
                        System.out.printf(ANSI_GREEN + "Updating %s...\n", clientUrl);
                        
                        List<Object> row = Arrays.asList(
                            column.get(0).toString(),
                            column.get(1).toString(),
                            column.get(2).toString(),
                            column.get(3).toString(),
                            column.get(4).toString(),
                            date,
                            version + versionNumber,
                            resp
                        );
                            
                        newValues.add(row);
                    }
                    else {
                        System.out.println(ANSI_YELLOW + "Skipping " + clientUrl + "!\n" + ANSI_RESET);

                        List<Object> row = Arrays.asList(
                            column.get(0).toString(),
                            column.get(1).toString(),
                            column.get(2).toString(),
                            column.get(3).toString(),
                            column.get(4).toString(),
                            date,
                            version + versionNumber,
                            "Skipped"
                        );
                    }
                }
                catch (Exception e) {
                    System.out.printf(ANSI_RED + e + "\n", column.get(2).toString(), column.size());
                    System.out.println(e);
                    System.out.println(ANSI_RED + "-----------------------------------------" + ANSI_RESET);
                }                
            }

            List<ValueRange> data = new ArrayList<>();
            data.add(new ValueRange()
                .setRange(range)
                .setValues(newValues));

            BatchUpdateValuesRequest updateBody = new BatchUpdateValuesRequest()
                    .setValueInputOption("RAW")
                    .setData(data);
            BatchUpdateValuesResponse updateResult =
                    service.spreadsheets().values().batchUpdate(spreadsheetId, updateBody).execute();
            System.out.printf("%d cells updated.", updateResult.getTotalUpdatedCells());
        }
    }

    private static String sendRequest(String url) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		// System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
            
            return response.toString();
		} else {
			System.out.println("GET request not worked");
            return "";
		}
	}
}